package de.julianreith.robotsort;

import de.danielkruse.robotsort.controller.interfaces.IOperationCompleted;
import de.danielkruse.robotsort.controller.interfaces.IRobotController;
import de.danielkruse.robotsort.controller.interfaces.ITaskCompleted;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public class RobotControllerImplementation implements IRobotController {

    @Override
    public void moveToChain( ITaskCompleted callback ) {
        System.out.println( "Move to prepare row" );
        callback.success();
    }

    @Override
    public void scanIsObjectInFront( IOperationCompleted<Boolean> callback ) {
        System.out.println( "Is an object in front of me?" );
        callback.success( false );
    }

    @Override
    public void moveSwap( boolean isOnLeftSide, ITaskCompleted callback ) {
        System.out.println( "Okay I have to swap the position now with the robot on the " + ( isOnLeftSide ? "left" : "right" ) + " side!" );
        callback.success();
    }

    @Override
    public void moveHighlightNoSwap( ITaskCompleted callback ) {
        System.out.println("Just highlighting that we don't have to swap! Maybe a dance?");
        callback.success();
    }
}
