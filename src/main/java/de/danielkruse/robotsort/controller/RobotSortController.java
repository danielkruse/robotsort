package de.danielkruse.robotsort.controller;

import de.danielkruse.robotsort.RobotSort;
import de.danielkruse.robotsort.controller.interfaces.IRobotInitializer;
import de.danielkruse.robotsort.controller.interfaces.ITaskCompleted;
import de.danielkruse.robotsort.network.packets.BubbleSortPacket;
import de.danielkruse.robotsort.network.packets.NextScanPacket;
import de.danielkruse.robotsort.network.packets.RequestWeightPacket;
import de.danielkruse.robotsort.network.packets.ResponseWeightPacket;
import de.danielkruse.robotsort.network.util.EnumReport;
import de.danielkruse.robotsort.state.EnumRobotState;
import de.danielkruse.robotsort.state.StateStorage;
import lombok.Getter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public class RobotSortController implements IRobotInitializer {

    private static final boolean SORT_ASC = true;

    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(0);

    @Getter
    private ScheduledFuture<?> scheduledFuture = null;

    private RobotSort robotSort;

    public RobotSortController(RobotSort robotSort) {
        this.robotSort = robotSort;
    }


    @Override
    public void setWeight(int weight) {
        // Set the value/weight of the robot
        this.robotSort.getStateStorage().weight = weight;
    }

    /**
     * Broadcast to all robots that we start to give every robot an order number
     * We start this operation with the index 0
     */
    @Override
    public void executeSort() {
        this.robotSort.getDatagramNetwork().report(EnumReport.DEBUG, "Sort executed");
        this.robotSort.getDatagramNetwork().broadcast(new NextScanPacket(0));
    }

    /**
     * Send a debug message to all robots
     *
     * @param message Debug report payload message
     */
    @Override
    public void debug(String message) {
        this.robotSort.getDatagramNetwork().report(EnumReport.DEBUG, message);
    }

    /**
     * Send a debug message to all robots
     *
     * @param json Json data payload message
     */
    @Override
    public void provideData(String json) {
        this.robotSort.getDatagramNetwork().report(EnumReport.DATA, json);
    }

    /**
     * Called when this robot is the next robot to prepare in the queue.
     * This robot is now the first one in the queue.
     *
     * @param index Id for this robot
     */
    public void prepareNext(int index) {
        // Save the robot id
        this.robotSort.getStateStorage().index = index;

        this.robotSort.getStateStorage().changeState(EnumRobotState.GO_TO_CHAIN);

        final int nextIndex = index + 1;

        // Move this robot to the left to make space for the next robot
        this.robotSort.getRobotController().moveToChain(new ITaskCompleted() {
            @Override
            public void success() {

                // Set robot in waiting mode for the sort
                robotSort.getStateStorage().changeState(EnumRobotState.WAIT_FOR_SORT);

                // Notify the next robot
                robotSort.getDatagramNetwork().broadcast(new NextScanPacket(nextIndex));

                // Timeout if no robot responds
                scheduledFuture = executor.schedule(new Runnable() {
                    @Override
                    public void run() {
                        robotSort.getDatagramNetwork().broadcast(new BubbleSortPacket(0));
                    }
                }, 10, TimeUnit.SECONDS);
            }
        });
    }

    /**
     * Start the bubble sort process for this robot.
     * He will request the robot next to him to swap!
     */
    public void startBubbleSort() {
        StateStorage storage = this.robotSort.getStateStorage();
        int partnerIndex = storage.index + 1;
        int weight = storage.weight;

        if (partnerIndex > storage.maxIndex) {
            if (storage.index == 0) {
                // The robot is alone in the network :(
                this.robotSort.getDatagramNetwork().report(EnumReport.INFORMATION, "I am alone in the network :(");
            } else {
                // Start the bubble sort from the beginning because we reached the end!
                this.robotSort.getDatagramNetwork().broadcast(new BubbleSortPacket(0));
            }
        } else {
            // Request the weight of the robot next to me!
            this.robotSort.getDatagramNetwork().broadcast(new RequestWeightPacket(partnerIndex, weight));
        }
    }

    /**
     * Incoming request of a robot that wants to swap
     *
     * @param requesterIndex  The index id of the robot that wants to swap
     * @param requesterWeight The weight of the robot that wants to swap
     */
    public void handleRequest(int requesterIndex, int requesterWeight) {
        int weight = this.robotSort.getStateStorage().weight;
        this.robotSort.getDatagramNetwork().broadcast(new ResponseWeightPacket(requesterIndex, weight));

        if (SORT_ASC ? requesterWeight > weight : requesterWeight < weight) {
            // Swap robot position
            swap(requesterIndex, true);
        } else {
            this.robotSort.getRobotController().moveHighlightNoSwap(new ITaskCompleted() {
                @Override
                public void success() {
                    // Okay, we don't have to swap, so just swap with the next one!
                    RobotSortController.this.startBubbleSort();
                }
            });
        }
    }

    /**
     * Incoming response of the requested robot
     *
     * @param requestedIndex  The index id of the requested robot
     * @param requestedWeight The weight of the requested robot
     */
    public void handleResponse(int requestedIndex, int requestedWeight) {
        int weight = this.robotSort.getStateStorage().weight;

        if (SORT_ASC ? requestedWeight < weight : requestedWeight > weight) {
            // Swap robot position
            swap(requestedIndex, false);
        } else {
            this.robotSort.getRobotController().moveHighlightNoSwap(new ITaskCompleted() {
                @Override
                public void success() {

                }
            });
        }
    }

    /**
     * Swapping the position of two robots
     *
     * @param swapWithIndex The id of the other robot
     * @param isOnLeftSide  Is the current robot on the left side?
     */
    private void swap(int swapWithIndex, boolean isOnLeftSide) {
        // Swap index of robot
        this.robotSort.getStateStorage().index = swapWithIndex;

        // Okay we have to swap positions now
        this.robotSort.getRobotController().moveSwap(isOnLeftSide, new ITaskCompleted() {
            @Override
            public void success() {
                // Okay I'm done with swapping, lets swap with the next one!
                RobotSortController.this.startBubbleSort();
            }
        });
    }

}
