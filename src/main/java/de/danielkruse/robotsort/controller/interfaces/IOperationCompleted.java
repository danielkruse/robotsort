package de.danielkruse.robotsort.controller.interfaces;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public interface IOperationCompleted<T> {
    /**
     * Operation completed with a result
     * @param result The dynamic result
     */
    void success(T result);
}
