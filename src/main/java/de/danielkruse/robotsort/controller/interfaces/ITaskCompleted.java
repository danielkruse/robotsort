package de.danielkruse.robotsort.controller.interfaces;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public interface ITaskCompleted {
    /**
     * Task successfully completed
     */
    void success();
}
