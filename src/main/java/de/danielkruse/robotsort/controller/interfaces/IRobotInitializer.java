package de.danielkruse.robotsort.controller.interfaces;

import de.danielkruse.robotsort.network.util.EnumReport;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public interface IRobotInitializer {

    /**
     * Set the weight value of the robot for the sorting
     *
     * @param weight Weight value for this robot
     */
    void setWeight(int weight);

    /**
     * Execute the whole progress
     */
    void executeSort();

    /**
     * Send a debug message to all robots
     *
     * @param message Debug report payload message
     */
    void debug(String message);

    /**
     * Send a debug message to all robots
     *
     * @param json Json data payload message
     */
    void provideData(String json);
}
