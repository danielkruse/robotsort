package de.danielkruse.robotsort.controller.interfaces;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public interface IRobotController {

    /**
     * Moves the robot to the chain for the sort
     * @param callback Task completed
     */
    void moveToChain(ITaskCompleted callback);

    /**
     * Detects if an object is in front of the robot
     * @param callback Operation result
     */
    void scanIsObjectInFront(IOperationCompleted<Boolean> callback);

    /**
     * Swapping the robot with another robot
     * @param isOnLeftSide This robot is coming from the left side and moves to the right
     * @param callback Task completed
     */
    void moveSwap( boolean isOnLeftSide, ITaskCompleted callback );

    /**
     * Just an highlighting event to show that this robot don't want to swap
     * @param callback Task completed
     */
    void moveHighlightNoSwap( ITaskCompleted callback );
}
