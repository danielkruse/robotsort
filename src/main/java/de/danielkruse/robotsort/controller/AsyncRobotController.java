package de.danielkruse.robotsort.controller;

import de.danielkruse.robotsort.controller.interfaces.IOperationCompleted;
import de.danielkruse.robotsort.controller.interfaces.IRobotController;
import de.danielkruse.robotsort.controller.interfaces.ITaskCompleted;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public class AsyncRobotController implements IRobotController {

    private Executor executor = Executors.newSingleThreadExecutor();
    private IRobotController robotController;

    public AsyncRobotController( IRobotController robotController ) {
        this.robotController = robotController;
    }

    @Override
    public void moveToChain( final ITaskCompleted callback ) {
        this.executor.execute(new Runnable() {
            @Override
            public void run() {
                AsyncRobotController.this.robotController.moveToChain(callback);
            }
        });
    }

    @Override
    public void scanIsObjectInFront( final IOperationCompleted<Boolean> callback ) {
        this.executor.execute(new Runnable() {
            @Override
            public void run() {
                AsyncRobotController.this.robotController.scanIsObjectInFront(callback);
            }
        });
    }

    @Override
    public void moveSwap( final boolean isOnLeftSide, final ITaskCompleted callback ) {
        this.executor.execute(new Runnable() {
            @Override
            public void run() {
                AsyncRobotController.this.robotController.moveSwap(isOnLeftSide, callback);
            }
        });
    }

    @Override
    public void moveHighlightNoSwap( final ITaskCompleted iTaskCompleted ) {
        this.executor.execute(new Runnable() {
            @Override
            public void run() {
                AsyncRobotController.this.robotController.moveHighlightNoSwap(iTaskCompleted);
            }
        });
    }
}
