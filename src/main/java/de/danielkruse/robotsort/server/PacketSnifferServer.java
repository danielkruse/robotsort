package de.danielkruse.robotsort.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.danielkruse.robotsort.network.PacketRegistry;
import de.danielkruse.robotsort.network.packets.*;
import de.danielkruse.robotsort.network.util.JsonDataPacket;
import de.danielkruse.robotsort.network.util.PacketHandler;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

public class PacketSnifferServer extends WebSocketServer {

    private static final int PORT = 25500;
    private static final int WEB_PORT = 25501;
    private static final Gson GSON = new GsonBuilder().setLenient().create();

    private List<WebSocket> webSockets = new ArrayList<>();

    private InetAddress address;
    private DatagramSocket socket;

    private PacketRegistry packetRegistry = new PacketRegistry();

    public static void execute() {
        final PacketSnifferServer server = new PacketSnifferServer();
        new Thread(new Runnable() {
            @Override
            public void run() {
                server.async();
            }
        }).start();
    }

    public PacketSnifferServer() {
        super(new InetSocketAddress(WEB_PORT));

        try {
            this.socket = new DatagramSocket(PORT);
            this.socket.setBroadcast(true);

            this.address = InetAddress.getByName("255.255.255.255");
        } catch (Exception error) {
            error.printStackTrace();
        }

        start();
        System.out.println("WebServer started on port " + WEB_PORT + "!");
    }

    public void async() {
        byte[] buffer = new byte[64000];
        DatagramPacket packetBuffer = new DatagramPacket(buffer, buffer.length, this.address, PORT);
        while (!this.socket.isClosed()) {
            try {
                // Read packet
                this.socket.receive(packetBuffer);

                // Get json string
                String json = new String(packetBuffer.getData(), 0, packetBuffer.getLength());

                JsonDataPacket jsonDataPacket = this.packetRegistry.createPacket(json);
                System.out.println("[Proxy] [" + jsonDataPacket.getClass().getSimpleName() + "] " + json);

                // Send to all websocket clients
                for (WebSocket socket : this.webSockets) {
                    socket.send(json);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        System.out.println("WebClient connected " + conn.getRemoteSocketAddress().getHostName());
        this.webSockets.add(conn);
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        System.out.println("WebClient disconnected " + conn.getRemoteSocketAddress().getHostName());
        this.webSockets.remove(conn);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        System.out.println("Received message: " + message);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        ex.printStackTrace();
    }

}
