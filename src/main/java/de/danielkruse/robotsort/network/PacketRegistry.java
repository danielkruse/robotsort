package de.danielkruse.robotsort.network;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import de.danielkruse.robotsort.network.packets.*;
import de.danielkruse.robotsort.network.util.JsonDataPacket;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public class PacketRegistry {

    private static final Gson GSON = new Gson();
    private Map<Integer, Class<? extends JsonDataPacket>> packets = new HashMap<>();

    public PacketRegistry() {

        // Register all packets with an unique id

        register(0, NextScanPacket.class);
        register(1, NextScanAckPacket.class);
        register(2, RequestWeightPacket.class);
        register(3, ResponseWeightPacket.class);
        register(4, BubbleSortPacket.class);
        register(5, ReportPacket.class);
        register(6, PositionConfirmedPacket.class);
    }

    /**
     * Create packet out of the json message
     *
     * @param json Json input message
     * @return Json data packet
     */
    public JsonDataPacket createPacket(String json) {
        try {
            // Get packet id
            JsonElement element = new JsonParser().parse(json);
            int id = element.getAsJsonObject().get("id").getAsInt();

            // Get class
            Class<? extends JsonDataPacket> clazz = this.packets.get(id);

            return GSON.fromJson(json, clazz);
        } catch (Exception error) {
            error.printStackTrace();
        }

        return null;
    }

    /**
     * Register a packet with the given id
     *
     * @param id    The unique id of the packet
     * @param clazz Packet class
     */
    private void register(int id, Class<? extends JsonDataPacket> clazz) {
        this.packets.put(id, clazz);
    }

    /**
     * Write the id of the packet to the object
     *
     * @param packet Packet to modify
     */
    public boolean writeId(JsonDataPacket packet) {
        int id = -1; // Default id

        // Get id
        Class clazz = packet.getClass();
        for (Map.Entry<Integer, Class<? extends JsonDataPacket>> entry : this.packets.entrySet()) {
            if (entry.getValue().equals(clazz)) {
                id = entry.getKey();
                break;
            }
        }

        // Overwrite id
        packet.id = id;

        // Success?
        return id != -1;
    }
}
