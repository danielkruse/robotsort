package de.danielkruse.robotsort.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.danielkruse.robotsort.RobotSort;
import de.danielkruse.robotsort.controller.interfaces.IOperationCompleted;
import de.danielkruse.robotsort.network.packets.*;
import de.danielkruse.robotsort.network.util.EnumReport;
import de.danielkruse.robotsort.network.util.JsonDataPacket;
import de.danielkruse.robotsort.network.util.PacketHandler;
import de.danielkruse.robotsort.state.EnumRobotState;

import java.net.*;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public class DatagramNetwork extends Thread implements PacketHandler {

    private static final int PORT = 25500;
    private static final Gson GSON = new GsonBuilder().setLenient().create();

    private RobotSort robotSort;

    private InetAddress broadcastAddress;
    private DatagramSocket socket;

    private PacketRegistry packetRegistry = new PacketRegistry();

    private String localAddress;
    private String mac;

    public DatagramNetwork(RobotSort robotSort) {
        this.robotSort = robotSort;

        try {
            // Get ip address
            InetAddress inetAddress = InetAddress.getLocalHost();
            this.localAddress = inetAddress.getHostAddress();

            // Get mac address
            NetworkInterface network = NetworkInterface.getByInetAddress(inetAddress);
            byte[] mac = network.getHardwareAddress();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                stringBuilder.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            this.mac = stringBuilder.toString();

            this.socket = new DatagramSocket(PORT);
            this.socket.setBroadcast(true);

            // Default broadcast address
            this.broadcastAddress = InetAddress.getByName("255.255.255.255");

            // Get broadcast address in this subnet
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = interfaces.nextElement();

                if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                    continue;
                }

                List<InterfaceAddress> list = networkInterface.getInterfaceAddresses();
                for (InterfaceAddress interfaceAddress : list) {
                    if (interfaceAddress != null && interfaceAddress.getBroadcast() != null) {
                        this.broadcastAddress = interfaceAddress.getBroadcast();
                    }
                }
            }

        } catch (Exception error) {
            error.printStackTrace();
        }

        report(EnumReport.DEBUG, "A robot joined the network!");

        start();
    }

    /**
     * Broadcast a packet to all robots
     *
     * @param jsonDataPacket The json packet
     */
    public void broadcast(JsonDataPacket jsonDataPacket) {
        try {
            if( this.packetRegistry.writeId(jsonDataPacket) ) {
                byte[] buffer = GSON.toJson(jsonDataPacket).getBytes();
                this.socket.send(new DatagramPacket(buffer, buffer.length, this.broadcastAddress, PORT));
            } else {
                report(EnumReport.ERROR, "Unknown packet handling: " + jsonDataPacket.getClass().getSimpleName());
            }
        } catch (Exception error) {
            error.printStackTrace();
        }
    }


    @Override
    public void run() {
        byte[] buffer = new byte[64000];
        DatagramPacket packetBuffer = new DatagramPacket(buffer, buffer.length, this.broadcastAddress, PORT);
        while (!this.socket.isClosed()) {
            try {
                // Read packet
                this.socket.receive(packetBuffer);

                // Get json string
                String json = new String(packetBuffer.getData(), 0, packetBuffer.getLength());

                // Create packet instance
                JsonDataPacket jsonDataPacket = this.packetRegistry.createPacket(json);

                // Handle packet
                if (jsonDataPacket == null) {
                    report(EnumReport.ERROR, "Invalid json: " + json);
                } else {
                    jsonDataPacket.handle(this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Close the udp packet listener
     */
    public void close() {
        this.socket.close();
    }

    @Override
    public void handleNextScan(NextScanPacket packet) {
        // Position index for this robot
        final int index = packet.getNextPositionIndex();

        // Robot is waiting for scan
        if (this.robotSort.getStateStorage().getRobotState() == EnumRobotState.WAIT_FOR_SCAN) {

            // Next round to get the first robot in the queue
            this.robotSort.getRobotController().scanIsObjectInFront(new IOperationCompleted<Boolean>() {
                @Override
                public void success(Boolean result) {
                    // Is someone in front of the robot?
                    if (!result) {
                        DatagramNetwork.this.broadcast(new PositionConfirmedPacket(index));

                        // Acknowledge the packet (We don't have to acknowledge the first scan because there is no robot before that)
                        if (index != 0) {
                            DatagramNetwork.this.broadcast(new NextScanAckPacket(index - 1));
                        }

                        // I'm the next robot!
                        robotSort.getSortController().prepareNext(index);
                    }
                }
            });
        }
    }

    @Override
    public void handleNextScanAck(NextScanAckPacket packet) {
        // Is the acknowledgement addressed to this robot?
        if (isRobotAddressed(packet.getTargetIndex())) {
            // Cancel the timeout
            this.robotSort.getSortController().getScheduledFuture().cancel(true);
        }

        // Store max position index for every robot
        this.robotSort.getStateStorage().maxIndex = packet.getTargetIndex() + 1;
    }

    @Override
    public void handleBubbleSort(BubbleSortPacket packet) {
        // Is the bubble sort addressed to this robot?
        if (isRobotAddressed(packet.getTargetIndex())) {
            this.robotSort.getSortController().startBubbleSort();
        }
    }

    @Override
    public void handleRequestWeight(RequestWeightPacket packet) {
        int requesterIndex = packet.getTargetIndex() - 1;

        // Is the request addressed to this robot?
        if (isRobotAddressed(packet.getTargetIndex())) {
            this.robotSort.getSortController().handleRequest(requesterIndex, packet.getRequesterWeight());
        }
    }

    @Override
    public void handleResponseWeight(ResponseWeightPacket packet) {
        int requestedIndex = packet.getTargetIndex() + 1;

        // Is the response addressed to this robot?
        if (isRobotAddressed(packet.getTargetIndex())) {
            this.robotSort.getSortController().handleResponse(requestedIndex, packet.getWeight());
        }
    }

    @Override
    public void handleReport(ReportPacket packet) {
        // Ignore
    }

    @Override
    public void handlePositionConfirmed(PositionConfirmedPacket packet) {
        // Ignore
    }

    /**
     * Is the given index the same index of this bot?
     *
     * @param index Index to compare
     * @return Index is matching with this robot
     */
    private boolean isRobotAddressed(int index) {
        return this.robotSort.getStateStorage().index == index;
    }

    public void report(EnumReport type, String message) {
        int index = this.robotSort.getStateStorage().index;

        ReportPacket packet = new ReportPacket(index, this.localAddress, this.mac, type, message);
        broadcast(packet);
    }
}
