package de.danielkruse.robotsort.network.packets;

import de.danielkruse.robotsort.network.util.JsonDataPacket;
import de.danielkruse.robotsort.network.util.PacketHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class NextScanPacket extends JsonDataPacket {

    private int nextPositionIndex;

    @Override
    public void handle(PacketHandler handler) {
        handler.handleNextScan(this);
    }
}
