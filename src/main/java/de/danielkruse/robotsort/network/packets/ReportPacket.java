package de.danielkruse.robotsort.network.packets;

import de.danielkruse.robotsort.network.util.EnumReport;
import de.danielkruse.robotsort.network.util.JsonDataPacket;
import de.danielkruse.robotsort.network.util.PacketHandler;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Daniel Kruse on 11/12/2019.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ReportPacket extends JsonDataPacket {

    private int index;
    private String address;
    private String mac;
    private EnumReport type;
    private String message;

    @Override
    public void handle(PacketHandler handler) {
        handler.handleReport(this);
    }
}
