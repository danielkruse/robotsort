package de.danielkruse.robotsort.network.util;

public enum EnumReport {
    ERROR,
    DEBUG,
    INFORMATION,
    DATA
}
