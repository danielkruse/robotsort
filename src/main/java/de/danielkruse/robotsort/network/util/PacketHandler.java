package de.danielkruse.robotsort.network.util;

import de.danielkruse.robotsort.network.packets.*;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public interface PacketHandler {

    void handleNextScan(NextScanPacket packet);

    void handleNextScanAck(NextScanAckPacket packet);

    void handleBubbleSort(BubbleSortPacket packet);

    void handleRequestWeight(RequestWeightPacket packet);

    void handleResponseWeight(ResponseWeightPacket packet);

    void handleReport(ReportPacket packet);

    void handlePositionConfirmed(PositionConfirmedPacket packet);
}
