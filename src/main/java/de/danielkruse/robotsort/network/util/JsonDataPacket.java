package de.danielkruse.robotsort.network.util;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
@Getter
@NoArgsConstructor
public abstract class JsonDataPacket {

    public int id;

    public abstract void handle(PacketHandler handler);
}
