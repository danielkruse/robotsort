package de.danielkruse.robotsort.state;

import lombok.Getter;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public class StateStorage {

    @Getter
    private EnumRobotState robotState = EnumRobotState.WAIT_FOR_SCAN;

    public int weight;
    public int index;
    public int maxIndex;

    public void changeState(EnumRobotState newState) {
        this.robotState = newState;
    }
}
