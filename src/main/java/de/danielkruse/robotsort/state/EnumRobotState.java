package de.danielkruse.robotsort.state;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public enum EnumRobotState {

    /**
     * The robot is waiting for the scan to check if he is at the first place
     */
    WAIT_FOR_SCAN,

    /**
     * The robot is in the moving to chain progress
     */
    GO_TO_CHAIN,

    /**
     * The robot is waiting for the next bubble sort command
     */
    WAIT_FOR_SORT
}
