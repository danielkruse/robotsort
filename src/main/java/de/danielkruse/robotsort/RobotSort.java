package de.danielkruse.robotsort;

import de.danielkruse.robotsort.controller.AsyncRobotController;
import de.danielkruse.robotsort.controller.interfaces.IRobotController;
import de.danielkruse.robotsort.controller.interfaces.IRobotInitializer;
import de.danielkruse.robotsort.network.DatagramNetwork;
import de.danielkruse.robotsort.network.packets.NextScanPacket;
import de.danielkruse.robotsort.controller.RobotSortController;
import de.danielkruse.robotsort.state.StateStorage;
import lombok.Getter;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
@Getter
public class RobotSort {

    private StateStorage stateStorage = new StateStorage();
    private DatagramNetwork datagramNetwork = new DatagramNetwork(this);
    private RobotSortController sortController = new RobotSortController(this);
    private AsyncRobotController robotController;

    public RobotSort(IRobotController robotController) {
        this.robotController = new AsyncRobotController(robotController);
    }

    public IRobotInitializer getInitializer() {
        return this.sortController;
    }

}
