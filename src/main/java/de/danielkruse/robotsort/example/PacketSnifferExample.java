package de.danielkruse.robotsort.example;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.danielkruse.robotsort.network.PacketRegistry;
import de.danielkruse.robotsort.network.packets.*;
import de.danielkruse.robotsort.network.util.JsonDataPacket;
import de.danielkruse.robotsort.network.util.PacketHandler;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;

public class PacketSnifferExample extends Thread implements PacketHandler {

    private static final int PORT = 25500;
    private static final Gson GSON = new GsonBuilder().setLenient().create();

    private InetAddress address;
    private DatagramSocket socket;

    private PacketRegistry packetRegistry = new PacketRegistry();

    public static void main(String[] args) {
        new PacketSnifferExample().start();
    }

    public PacketSnifferExample() {
        try {
            this.socket = new DatagramSocket(PORT);
            this.socket.setBroadcast(true);

            this.address = InetAddress.getByName("255.255.255.255");
        } catch (Exception error) {
            error.printStackTrace();
        }
    }

    @Override
    public void run() {
        byte[] buffer = new byte[64000];
        DatagramPacket packetBuffer = new DatagramPacket(buffer, buffer.length, this.address, PORT);
        while (!this.socket.isClosed()) {
            try {
                // Read packet
                this.socket.receive(packetBuffer);

                // Get json string
                String json = new String(packetBuffer.getData(), 0, packetBuffer.getLength());

                // Create packet instance
                JsonDataPacket jsonDataPacket = this.packetRegistry.createPacket(json);

                // Handle packet
                jsonDataPacket.handle(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void handleNextScan(NextScanPacket packet) {
        System.out.println("handleNextScan " + packet.getNextPositionIndex());
    }

    @Override
    public void handleNextScanAck(NextScanAckPacket packet) {
        System.out.println("handleNextScanAck " + packet.getTargetIndex());
    }

    @Override
    public void handleBubbleSort(BubbleSortPacket packet) {
        System.out.println("handleBubbleSort " + packet.getTargetIndex());
    }

    @Override
    public void handleRequestWeight(RequestWeightPacket packet) {
        System.out.println("handleRequestWeight " + packet.getRequesterWeight() + " " + packet.getTargetIndex());
    }

    @Override
    public void handleResponseWeight(ResponseWeightPacket packet) {
        System.out.println("handleResponseWeight " + packet.getWeight() + " " + packet.getTargetIndex());
    }

    @Override
    public void handleReport(ReportPacket packet) {
        System.out.println("handleReport " + packet.getIndex() + " " + packet.getType() + " " + packet.getMessage());
    }

    @Override
    public void handlePositionConfirmed(PositionConfirmedPacket packet) {
        System.out.println("handlePositionConfirmed " + packet.getIndex() );
    }
}
