package de.danielkruse.robotsort.example;

import de.danielkruse.robotsort.RobotSort;
import de.danielkruse.robotsort.controller.interfaces.IRobotInitializer;
import de.julianreith.robotsort.RobotControllerImplementation;

/**
 * Created by Daniel Kruse on 22/10/2019.
 */
public class RobotSortExample {

    public static void main(String[] args) throws InterruptedException {

        // Create implementation
        RobotControllerImplementation robotControllerImplementation = new RobotControllerImplementation();

        // Create robot
        RobotSort robotSort = new RobotSort(robotControllerImplementation);

        // Setup robot
        IRobotInitializer initializer = robotSort.getInitializer();
        initializer.setWeight( 0 );
        initializer.executeSort();
    }
}
