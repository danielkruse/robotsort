import de.danielkruse.robotsort.server.PacketSnifferServer;

public class Start {
    public static void main(String[] args) {
        PacketSnifferServer.execute();
    }
}
